import fetch from "isomorphic-unfetch";

import Template from "../components/Template";


import styled from "styled-components";

import { VerticalText } from '../components/Styles/Verticals'
import NowPlaying from '../components/NowPlaying'
import Upcoming from '../components/Upcoming'

import Head from "next/head";



const THE_MOVIEDB_API_KEY = "20ac754dbe6ba584a7a8a8690c755249";







const Index = ({ nowPlaying, upcoming }) => (
  <Template title="Star Cines - INICIO">
    <Head>
      <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
    </Head>


    
      <NowPlaying movies={nowPlaying} />
    

    <div className="relative d-flex">
      <VerticalText style={{ backgroundColor: '#2998e2', marginBottom: 5 }}><h2 className="ma-0 pa-0 f-21">PROXIMAMENTE</h2></VerticalText>

      <Upcoming movies={upcoming} />



    </div>


  </Template>
);

Index.getInitialProps = async ({ req }) => {
  const res = await fetch(
    `https://api.themoviedb.org/3/movie/now_playing?api_key=${THE_MOVIEDB_API_KEY}&language=es-EC&page=1`
  );
  const json = await res.json();

  const res2 = await fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${THE_MOVIEDB_API_KEY}&language=en-US&page=1`
  );
  const json2 = await res2.json();



  return { nowPlaying: json.results, upcoming: json2.results };
};

export default Index;
