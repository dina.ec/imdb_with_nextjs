import styled from "styled-components";
import NProgress from 'nprogress';
import Router from 'next/router';
import Link from "next/link";


Router.onRouteChangeStart = () => {
  console.log('onRouteChangeStart')
  NProgress.start();
};
Router.onRouteChangeComplete = () => {
  console.log('onRouteChangeComplete')
  NProgress.done();
};

Router.onRouteChangeError = () => {
  console.log('onRouteChangeError')
  NProgress.done();
};



const NavStyled = styled.div`
  background-color: "#fff";
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 0px;
  padding-right: 20px;




  .menu a.item {
    text-decoration: none;
    color: #546E7A;
    font-size: 1.3rem;
    padding: 20px 20px;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }

  .menu a.item:hover,
  .menu a.item.active {
    color: #fff;
    background-color: #ea4737;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }
`;

const Badge = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 15px;
  border: 5px solid #07131B;
  position: absolute;
  top: -5px;
  right: -10px;
  color: #fff;
  background-color: #00c853;
  font-weight: bold;
`;

const Nav = ({ index = 0 }) => (
    <NavStyled>
    <img height="60" src="https://dina.ec/public_content/demos/star-cines/StarCines-logo.png" alt="" />
    <div className="menu">
      <Link href="/">
        <a className={`item ${index === 0 ? "active" : ""}`}>Inicio </a>
      </Link>
      <Link href="/">
        <a className="item">Cartelera </a>
      </Link>
      <Link href="/">
        <a className="item">Proximamente </a>
      </Link>
      <Link href="/">
        <a className="item">Precios </a>
      </Link>
      <Link href="/">
        <a className="item">Promociones </a>
      </Link>
    </div>

    <div className="d-flex ai-center">
      <img
        width="25"
        src="/static/svg/search.svg"
        alt="imdb search"
      />

      <div className="relative ma-left-20">
        <img
          className="ma-top-5"
          width="45"
          src="/static/svg/profile-photo.svg"
          alt="imdb search"
        />
        <Badge className="d-flex ai-center jc-center f-13 lh-200">3</Badge>
      </div>
    </div>
  </NavStyled>
);

export default Nav;
