import React, { Component } from "react";
import Nav from "./Nav";

import Head from "next/head";

import("../pages/style.css");

import styled from 'styled-components';





export default class Template extends Component {
  render() {
    const { children, title } = this.props;
    return (
      <div style={{ backgroundColor: '#07131B' }}>
        <Head>
          <title>{title}</title>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link
            rel="stylesheet"
            href="https://dina.ec/public_content/dina.css.gz"
          />
        </Head>
        <div>
          <Nav />
          <div>{children}</div>
        </div>
      </div>
    );
  }
}
