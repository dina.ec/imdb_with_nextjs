import React, { Component } from 'react'

import Movie from "./Movie";
import { VerticalNext, VerticalPrev } from './Styles/Verticals'

const THE_MOVIEDB_IMG = "https://image.tmdb.org/t/p/w500/";

export default class NowPlaying extends Component {



    state = {
        counter: 0
    }


    scroll = (id, direction) => {

        const { counter } = this.state;

        var tmpCounter = counter;
        if (direction == -1) {
            tmpCounter = document.getElementById(id).scrollLeft;

            const dx = tmpCounter > document.getElementById(id).offsetWidth / 2 ? 10 : 3;

            const interval = setInterval(() => {
                tmpCounter -= dx;
                document.getElementById(id).scrollLeft = tmpCounter;
                if (tmpCounter <= 0) {
                    clearInterval(interval);
                    this.setState({ counter: tmpCounter })
                }
            }, 1)
        } else {
            let far = document.getElementById(id).offsetWidth / 2 * direction;
            let pos = document.getElementById(id).scrollLeft + far - 400;


            const interval = setInterval(() => {
                tmpCounter += 3;

                document.getElementById(id).scrollLeft = tmpCounter;
                if (tmpCounter >= pos) {
                    clearInterval(interval);
                    this.setState({ counter: tmpCounter })
                }
            }, 1)
        }

    }

    render() {
        return (<>
            <div id="nowPlaying" className="d-flex" style={{ overflowX: "hidden" }}>
                {this.props.movies.map((item) => (
                    <Movie
                        key={item.id}
                        image={THE_MOVIEDB_IMG + item.poster_path}
                        title={item.title}
                    />
                ))}
            </div>

            {this.state.counter > 0 && (<VerticalPrev style={{ bottom: 5 }} onClick={() => this.scroll('nowPlaying', -1)} className="d-flex ai-center jc-center">
                <img width="10" src="/static/svg/next.svg" style={{ transform: 'rotate(180deg)' }} />
            </VerticalPrev>)}


            <VerticalNext onClick={() => this.scroll('nowPlaying', 1)} className="d-flex ai-center jc-center" style={{ bottom: 5 }}>
                <img width="10" src="/static/svg/next.svg" />
            </VerticalNext>
        </>)
    }
}