import React, { Component } from 'react'

import Movie from "./Movie";
import { VerticalNext, VerticalPrev, VerticalText } from './Styles/Verticals'

const THE_MOVIEDB_IMG = "https://image.tmdb.org/t/p/w500/";

import Slider from "react-slick";


import styled from 'styled-components';

const Funciones = styled.div`
width:100%;
color: #fff;
text-align:center;




.trow{
    display:grid;
    grid-template-columns: 1fr 2fr 2fr 2fr 2fr;
    grid-gap:10px;
    padding-top:5px;
    padding-bottom:5px;
    border-bottom:1px solid #546E7A;
}



`

const SLIDES_TO_SHOW = 4;

export default class Upcommig extends Component {



    state = {
        counter: 0,
        slideIndex: 0,
        movie: this.props.movies[0]
    }


    afterSlide = index => this.setState({ slideIndex: index });





    render() {

        const { slideIndex, movie } = this.state


        const movies = this.props.movies;



        var settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: false,
            arrows: false,

        };


        return (
            <div>

                <div className=" relative w-100 ma-bottom-5">






                    <div className="right relative w-100" style={{
                        height: '80vh', backgroundImage: `url(${THE_MOVIEDB_IMG + movie.backdrop_path})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover'
                    }}>
                        <div className="absolute left-0 right-0 top-0 bottom-0" style={{ backgroundColor: 'rgba(0,0,0,0.4)' }}>

                        </div>

                        <div className='absolute  right-10 bottom-20  pa-10 w-50 ' style={{ backgroundColor: 'rgba(38,50,56,0.8)', borderRadius: 10 }}>
                            <h2 className='ma-bottom-0 uppercase f-25 c-white font-roboto'>{movie.title}</h2>
                            <p className='pa-0 ma-0' style={{ color: 'rgb(255, 217, 0)' }}>12 años | Acción</p>
                            <p className="f-16 koho t-justify " style={{ color: '#fff' }}>{movie.overview.substr(0, 350)}...</p>

                            <Funciones>
                                <div className='trow font-roboto f-bold'>
                                    <div><b>#</b></div>
                                    <div><b>HORA</b></div>
                                    <div><b>DIMESIÓN</b></div>
                                    <div><b>IDIOMA</b></div>
                                    <div><b>ENTRADAS</b></div>
                                </div>
                                <div className='trow'>
                                    <div>1</div>
                                    <div>11H00</div>
                                    <div>2D</div>
                                    <div>Español</div>
                                    <div>AGOTADO</div>
                                </div>


                                <div className='trow'>
                                    <div>2</div>
                                    <div>13H30</div>
                                    <div>3D</div>
                                    <div>Español</div>
                                    <div>AGOTADO</div>
                                </div>


                                <div className='trow'>
                                    <div>3</div>
                                    <div>15H00</div>
                                    <div>3D</div>
                                    <div>Español</div>
                                    <div><a href="#" className='decoration-none pa-5' style={{ color: '#fff', backgroundColor: '#00E676' }}>DISPONIBLES</a></div>
                                </div>

                            </Funciones>
                        </div>
                    </div>










                </div>




            </div>

        )
    }
}
