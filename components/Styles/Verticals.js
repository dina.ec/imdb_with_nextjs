import styled from "styled-components";

const VerticalText = styled.div`
  writing-mode: tb-rl;
  transform: rotate(180deg);
  color: #fff;
  letter-spacing: 8.55px;
  text-align: center;
  line-height: 20px;
  padding-left:5px;
  padding-right:5px;

  
`;




const VerticalNext = styled.div`
  background-color: rgba(0,0,0,0.4);
  width:30px;
  position:absolute;
  right: 0;
  top: 0;
  bottom: 0px;
  cursor:pointer;

  :hover{
     background-color: rgba(0,0,0,0.6);
  }

`;


const VerticalPrev = styled.div`
  background-color: rgba(0,0,0,0.4);
  width:30px;
  position:absolute;
  left: 0;
  top: 0;
  bottom: 0px;
  cursor:pointer;

  :hover{
     background-color: rgba(0,0,0,0.6);
  }

  img{
      transform:rotateX('90deg')
  }

`;


export { VerticalNext, VerticalPrev, VerticalText }