import styled from 'styled-components';


const MovieStyled = styled.div`
position:relative;
margin-left:3px;





.hover-content{
  opacity:0;
  display: flex;
  flex-direction:column;
  justify-content:center;
  align-items:center;
  position:absolute;
  left:0;
  right:0;
  bottom: 5px;
  top:0;
  background-color:rgba(255,0,0,0.8);
}

.hover-content:hover{
  opacity:1 !important;
   -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

.hover-content .btn{
  display:flex;
  width:100%;
  cursor: pointer;
   flex-direction:column;
  justify-content:center;
  align-items:center;
  color: #fff;
  flex:1;
}


.hover-content .btn:hover{
 background-color:rgba(255,0,0,1);
  -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}



`


const Movie = ({ image, title }) => (
  <MovieStyled>
    <img className="of-cover" width="200" height="290" src={image} tag={title} />
    <div className="hover-content">
      <div className="btn">
        <img width="30" src="/static/svg/info.svg" />
        <p className="pa-5 ma-0">Información</p>
      </div>
      <div className="btn">
        <img width="30" src="/static/svg/ticket.svg" />
        <p className="pa-5 ma-0">Comprar Tickets</p>
      </div>
      <div className="btn">
        <img width="30" src="/static/svg/heart.svg" />
        
        <p className="pa-5 ma-0">Agregar a favoritos</p>
      </div>
    </div>
  </MovieStyled>
);

export default Movie;
